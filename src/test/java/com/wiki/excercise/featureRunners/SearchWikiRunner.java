package com.wiki.excercise.featureRunners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)

@CucumberOptions(
        format = {"pretty", "html:target/cucumber", "json:target/SearchWikiRunner.json"},
        features = {"src/test/resources/features/searching_in_wikipedia.feature"},
        strict = true,
        glue = {"classpath:com.wiki.excercise"}
)

public class SearchWikiRunner {
}
