package com.wiki.excercise.serviceUklocation;

import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class UkLocation {

    private static final String PATH = "http://www.webservicex.net/uklocation.asmx?WSDL";
    private static final String ByCOUNTY_PATH = "http://www.webservicex.net/uklocation.asmx/GetUKLocationByCounty";

    @Test
    public void checkServiceUkLocation(){

        given().that().
                when().
                get(PATH).
                then().
                assertThat().statusCode(200);
    }

    @Test
    public void checkPortSoap(){

        String ukLocation = given().param("County","Essex").
                when().
                get(ByCOUNTY_PATH).then().
                statusCode(200).extract().xmlPath().getString("NewDataSet").toString();
        System.out.println(ukLocation);

    }


}
