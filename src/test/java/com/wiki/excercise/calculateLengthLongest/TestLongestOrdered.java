package com.wiki.excercise.calculateLengthLongest;


import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestLongestOrdered {
    LongestOrdered longestOrdered = new LongestOrdered();

    @Test
    public void checkTheLengthOfLongestOrdered() {

        int example1[] = {1, 4, 1, 4, 2, 1, 3, 5, 6, 2, 3, 7};

        int example2[] = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5};

        int example3[] = {2, 7, 1, 8, 2, 8, 1};

        assertTrue(longestOrdered.getMaxLength(example1) == 4);
        assertTrue(longestOrdered.getMaxLength(example2) == 3);
        assertTrue(longestOrdered.getMaxLength(example3) == 2);


    }
}
