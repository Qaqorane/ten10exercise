package com.wiki.excercise.calculateLengthLongest;

public class LongestOrdered {

    public int getMaxLength(int[] myArray) {
        if (myArray == null || myArray.length <= 1)
            return myArray.length;
        int maxLength = 1;
        int curLength = 1;

        for (int i = 1; i < myArray.length; i++) {
            if (myArray[i] > myArray[i - 1])
                curLength++;
            else {
                if (maxLength < curLength)
                    maxLength = curLength;
                curLength = 1;
            }

        }
        if (maxLength < curLength)
            return curLength;
        return maxLength;
    }

}
