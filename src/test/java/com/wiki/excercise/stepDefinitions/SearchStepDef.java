package com.wiki.excercise.stepDefinitions;


import com.wiki.excercise.pageObjects.WikipediaPage;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertTrue;

public class SearchStepDef extends WikipediaPage {


    WebDriver driver = getDriver();


    @Given("^user is on wikipedia landing page$")
    public void user_is_on_wikipedia_landing_page() throws Throwable {
        openBrowser();
    }

    @Given("^user types \"([^\"]*)\" in the search input field$")
    public void user_types_in_the_search_input_field(String word) throws Throwable {
        enterSearchWord(word);

    }

    @Given("^user selects \"([^\"]*)\" as the search language$")
    public void user_selects_as_the_search_language(String option) throws Throwable {
        selectLanguage(option);
    }

    @When("^user clicks the search button$")
    public void user_clicks_the_search_button() throws Throwable {
        clickSearch();

    }

    @Then("^search results page should be available in desired language$")
    public void search_results_page_should_be_available_in_desired_language() throws Throwable {
        assertTrue("Validate that first heading matches the search string", checkFirstHeading());
        assertTrue("Verify that result page is in a language given as parameter", checkSelectedLanguage());

    }

    @Then("^search result page should contain link to version of desired language$")
    public void search_result_page_should_contain_link_to_version_of_desired_language() throws Throwable {
        navigateToSearchResult();
        assertTrue("validate search results page includes link to version in eng", isLinVersionLinkToEnglishPresent());
    }

    @After
    public void closeBrowser() {
        tearDown();
    }
}
