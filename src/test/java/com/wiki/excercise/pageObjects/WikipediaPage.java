package com.wiki.excercise.pageObjects;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

public class WikipediaPage {

    public static final String WIKIPEDIA_PAGE = "https://www.wikipedia.org/";
    public static final String CHROME_MAXIMISED = "--start-maximized";
    String selectedLanguage = "";
    String searchedWord = "";

    protected static WebDriver driver;


    //locators
    private By searchLanguage = By.cssSelector("#searchLanguage");
    private By searchInput = By.cssSelector("#searchInput");
    private By submit_btn = By.cssSelector("#search-form button");
    private By firstHeading = By.cssSelector("#firstHeading");
    private By searchResultLink = By.cssSelector("#mw-content-text > div > div:nth-child(1)  a");

    public WebDriver getDriver() {
        if (driver == null) {
            setUp();
        }
        return driver;
    }

    public void openBrowser() {
        driver.get(WIKIPEDIA_PAGE);
    }

    public void selectLanguage(String lang) {
        selectedLanguage = lang;
        WebElement element = driver.findElement(searchLanguage);
        Select language = new Select(element);
        language.selectByValue(lang);
    }

    public void enterSearchWord(String word) {
        searchedWord = word;
        elementToBeVisible(searchInput);
        driver.findElement(searchInput).sendKeys(word);
    }

    public void clickSearch() {
        driver.findElement(submit_btn).click();
    }


    public static Object executeScript(String script, Object... elements) {
        return ((JavascriptExecutor) driver).executeScript(script, elements);
    }


    public boolean checkFirstHeading() {
        boolean outcome = false;
        if (driver.findElement(firstHeading).getText().equalsIgnoreCase(searchedWord)) {
            outcome = true;
        }
        return outcome;
    }

    public boolean checkSelectedLanguage() {
        boolean outcome = false;
        if (driver.findElement(firstHeading).getAttribute("lang").equals(selectedLanguage)) {
            outcome = true;
        }
        return outcome;
    }

    public boolean isLinVersionLinkToEnglishPresent() {
        return (driver.getPageSource().contains("https://en.wikipedia.org/wiki/Cheetah"));
    }


    public void navigateToSearchResult() {
        String currentUrl = driver.getCurrentUrl();
        driver.get(currentUrl);
        sleepTime(3);

    }

    public void setUp() {
//       Chromedriver executable available on your path -
        File driverPath = new File("tools/webdrivers/chromedriver");
        System.setProperty("webdriver.chrome.driver", driverPath.toString());
        ChromeOptions browserOptions = new ChromeOptions();
        browserOptions.addArguments(CHROME_MAXIMISED);
        driver = new ChromeDriver(browserOptions);
        driver.manage().deleteAllCookies();
    }

    public void elementToBeVisible(By selector) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
    }

    public void sleepTime(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void tearDown() {
        driver.quit();
    }


}
