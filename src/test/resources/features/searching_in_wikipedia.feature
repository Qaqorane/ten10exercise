Feature: searching in wikipedia
  As wikipedia user I would like to search
  So I am able to use the free online encyclopedia

  Scenario: Search for a given string in English
    Given user is on wikipedia landing page
    And user types "cheetah" in the search input field
    And user selects "en" as the search language
    When user clicks the search button
    Then search results page should be available in desired language
    And  search result page should contain link to version of desired language









