
### completed exercises project

**Exercises** 

1-Programming 
:   Calculate the length of the longest ordered sub-sequence within the array.

2-Web Front End testing 
:   cucumber-jvm and selenium-webdriver to run browser-based UI automated tests in desktop Chrome.
    you'll need Chrome (or Chromium) installed.

3-Web Service Test 
:   Automate the retrieval of a UK location from a county given as parameter


**Note**
Requires are
- JDK 1.8 
- Test can be run with IDE using feature Runners 
- to run test with Maven use maven commands 
you'll need Chrome (or Chromium) installed. 